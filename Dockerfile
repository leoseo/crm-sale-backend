FROM node:16.13.0

ENV PORT=4000

# Create app directory
WORKDIR /usr/src/app

# Copy files first
#COPY package*.json tsconfig*.json build.ts ./
#COPY dist/ ./dist
COPY . .

# installing and building
RUN pwd && node --version && npm --version && npm install && npm run build && ls -la /usr/src/app

EXPOSE ${PORT}
CMD [ "npm", "start" ]
