import {ApolloServer, AuthenticationError} from 'apollo-server-express';
import {
    ApolloServerPluginLandingPageLocalDefault,
    ApolloServerPluginLandingPageProductionDefault
} from "apollo-server-core";
import bookResolvers from "@resolvers/book.resolver";
import accountResolvers from "@resolvers/account.resolver";
import baseTypeDefs from "@typedefs/base.type.def";
import bookTypeDefs from "@typedefs/book.type.def";
import accountTypeDefs from "@typedefs/account.type.def";
import roleResolvers from "@resolvers/role.resolver";
import roleTypeDefs from "@typedefs/role.type.def";

import {merge} from 'lodash';
import {verifyAuthorizationToken} from "@shared/jwt.helper";
import {JwtPayload} from "jsonwebtoken";
import {EMPTY} from "@shared/constants";
import cookieParser from "cookie-parser";
import cors from "cors";
import {corsConfiguration} from "@shared/cors.configuration";

import express from 'express';
import 'express-async-errors';

import loginRouter from "./routes/login.route";


async function startApolloServer() {

    // Apollo Server Express configuration
    const server = new ApolloServer({
        typeDefs: [baseTypeDefs, bookTypeDefs, accountTypeDefs, roleTypeDefs],
        resolvers: merge({}, bookResolvers, accountResolvers, roleResolvers),
        // cors: corsConfiguration, // using apollo-server-express instead of apollo-server
        plugins: [
            // Install a landing page plugin based on NODE_ENV
            process.env.NODE_ENV === 'production'
                ? ApolloServerPluginLandingPageProductionDefault({
                    // graphRef: "my-graph-id@my-graph-variant",
                    footer: false,
                })
                : ApolloServerPluginLandingPageLocalDefault({
                    footer: false
                }),
        ],
        context: async (context) => {
            // destructuring
            const {req} = context;
            if (req) {
                try {
                    const payload: JwtPayload = await verifyAuthorizationToken(
                        req.headers.authorization || EMPTY) as JwtPayload;
                    return payload.username as object;
                } catch (e) {
                    throw new AuthenticationError('User must be logged in first!');
                }
            }
        }
    });

    // Express server configuration
    const app = express();
    app.use(express.json());
    app.use(express.urlencoded({extended: true}));
    app.use(cookieParser());
    app.use(cors(corsConfiguration));
    app.use(loginRouter);

    // applying middleware and await server start
    await server.start();
    server.applyMiddleware({app, path: "/"});

    return {server, app};
}

export default startApolloServer;
