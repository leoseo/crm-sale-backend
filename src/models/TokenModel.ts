interface ITokenModel {
    username: string,
    accessToken: string,
    tokenType: string,
    refreshToken: string,
    scope: string,
    expiresIn: number
}

export default class TokenModel implements ITokenModel {
    username: string;
    accessToken: string;
    expiresIn: number;
    refreshToken: string;
    scope: string;
    tokenType: string;

    constructor(username: string, accessToken: string, expiresIn: number,
                refreshToken: string, tokenType: string) {
        this.username = username;
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.refreshToken = refreshToken;
        this.scope = 'openid';
        this.tokenType = tokenType;
    }
}