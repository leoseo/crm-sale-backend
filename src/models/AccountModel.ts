
interface IAccountModel {
    fullName: string,
    email: string,
    phone: string,
    username: string,
    password: string,
    department : string,
    accStatus : string
}

export default class AccountModel implements IAccountModel {

    public accStatus: string;
    public department: string;
    public email: string;
    public fullName: string;
    public password: string;
    public phone: string;
    public username: string;

    constructor(accStatus: string, department: string, email: string,
                fullName: string, password: string, phone: string, username: string) {
        this.accStatus = accStatus;
        this.department = department;
        this.email = email;
        this.fullName = fullName;
        this.password = password;
        this.phone = phone;
        this.username = username;
    }
}
