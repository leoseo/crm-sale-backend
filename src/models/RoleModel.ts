export interface IRoleModel {
    id: number,
    name: string,
    type: number,
    description: string,
    createdAt: string,
    updatedAt: string,
    deletedAt: string
}

export default class RoleModel implements IRoleModel {

    id: number;
    name: string;
    type: number;
    description: string;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;

    constructor( id: number, name: string, type: number, description: string) {
        this.description = description;
        this.id = id;
        this.name = name;
        this.type = type;
        this.createdAt = '';
        this.updatedAt = '';
        this.deletedAt = '';
    }
}