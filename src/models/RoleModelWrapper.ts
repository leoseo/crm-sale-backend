import RoleModel from "@models/RoleModel";

export interface RoleModelWrapper {
   roleModel: RoleModel
}