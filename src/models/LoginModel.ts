export interface LoginModel {
    username: string
    password: string
}

export interface RefreshModel {
    refreshToken: string
    username: string
}