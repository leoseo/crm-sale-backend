import {Request, Response, Router} from "express";
import {StatusCodes} from "http-status-codes";

import AccountService from "@services/account.service";
import logger from "@shared/logger";
import {LoginModel, RefreshModel} from "@models/LoginModel";

const accountService = new AccountService();

const loginRouter = Router();

loginRouter.post('/login', (req: Request, res: Response) => {
    const loginModel: LoginModel = req.body;
    logger.info(`Getting login request from ${JSON.stringify(loginModel)}`)
    accountService.login(loginModel.username, loginModel.password)
        .then(token => {
            logger.info(`Successfully login for username ${loginModel.username}!`)
            res.status(StatusCodes.OK).json(token).send();
        });
});

loginRouter.post('/refreshToken', (req: Request, res: Response) => {
    const refreshModel: RefreshModel = req.body;
    logger.info(`Getting refresh token request from ${JSON.stringify(refreshModel)}`)
    accountService.refreshToken(refreshModel.refreshToken, refreshModel.username)
        .then(token => {
            logger.info(`Successfully refresh token for username ${refreshModel.username}!`)
            res.status(StatusCodes.OK).json(token).send();
        });
});

export default loginRouter;
