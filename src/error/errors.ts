import {CustomError} from "./CustomError";
import {Error} from "sequelize";
import logger from "@shared/logger";

export const AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED'
export const EMPTY_INPUT = 'EMPTY_INPUT'
export const STORE_TOKEN_FAILURE = 'STORE_TOKEN_FAILURE'
export const INVALID_REFRESH_TOKEN = 'INVALID_REFRESH_TOKEN'
export const EMPTY_MANDATORY_FIELD = 'EMPTY_MANDATORY_FIELD'
export const UNKNOWN_ERROR = 'UNKNOWN_ERROR'

export const errorHandler = (e: unknown, specificErr: string, errorCode: string) => {
    if (e instanceof CustomError) {
        logger.error(e.stack)
        throw e;
    } else if (e instanceof Error){
        logger.error(specificErr, e.stack)
        throw new CustomError(e.message, errorCode)
    } else {
        logger.error(e);
        throw new CustomError('Unknown error', UNKNOWN_ERROR)
    }
}