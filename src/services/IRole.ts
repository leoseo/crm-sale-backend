import RoleModel from "@models/RoleModel";

export interface IRole {

    create: (roleModel: RoleModel) => Promise<RoleModel>;
    update: (roleModel: RoleModel) => Promise<RoleModel>;
    // disable: (id: number) => Promise<string>;
}