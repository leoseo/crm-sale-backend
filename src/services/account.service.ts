import {IAccount} from "./IAccount";
import AccountModel from "@models/AccountModel";
import logger from "@shared/logger";
import {formatCacheKey} from "@shared/functions";
import {account} from "../graphql/sample.data";

import {
    generateAccessToken,
    generateRefreshToken,
    EXPIRES_IN_SECONDS,
    TOKEN_TYPE
} from "@shared/jwt.helper";
import TokenModel from "@models/TokenModel";
import {CustomError} from "../error/CustomError";
import {
    AUTHENTICATION_FAILED,
    EMPTY_INPUT,
    STORE_TOKEN_FAILURE,
    errorHandler, INVALID_REFRESH_TOKEN
} from "../error/errors";
import NodeCache from 'node-cache';

const nodeCache = new NodeCache();

export default class AccountService implements IAccount {

    public async registerAccount(accountModel: AccountModel): Promise<AccountModel> {
        // no need since AD manage it
        logger.info(`Registering account based on ${JSON.stringify(accountModel)}`);
        return Promise.resolve(account);
    }

    public async login(username: string, password: string): Promise<TokenModel | null> {

        try {
            // change to call AD
            // hard code to OK

            if (!username || !password) {
                throw new CustomError('Empty username or password!', EMPTY_INPUT)
            }

            return await this.generateTokens(username);
        } catch (e: unknown) {
            errorHandler(e, `Failed to authenticate for user ${username}:`,
                AUTHENTICATION_FAILED);
        }
        return null;
    }

    public async refreshToken(refreshToken: string, username: string): Promise<TokenModel | null> {
        try {
            // change to call AD
            if (!username || !refreshToken) {
                throw new CustomError('Empty username or refresh token!', EMPTY_INPUT)
            }

            const cacheRefreshToken = nodeCache.get(formatCacheKey(username));
            if (cacheRefreshToken == undefined || cacheRefreshToken != refreshToken) {
                throw new CustomError(`Invalid refresh token ${refreshToken}`,
                    INVALID_REFRESH_TOKEN)
            }

            return await this.generateTokens(username);

        } catch (e: unknown) {
            errorHandler(e, `Failed to authenticate for user ${username}:`,
                AUTHENTICATION_FAILED);
        }
        return null;
    }

    private async generateTokens(username: string): Promise<TokenModel> {
        const accessToken: string = await generateAccessToken(username);
        const refreshToken: string = await generateRefreshToken(username);
        const success: boolean = nodeCache.set(formatCacheKey(username), refreshToken,
            EXPIRES_IN_SECONDS) || false;
        if (!success) {
            throw new CustomError(`Failed to store refresh token for ${username}!`,
                STORE_TOKEN_FAILURE);
        }
        return new TokenModel(username, accessToken, EXPIRES_IN_SECONDS, refreshToken, TOKEN_TYPE);
    }

}