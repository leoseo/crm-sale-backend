import {IRole} from "@services/IRole";
import RoleModel from "@models/RoleModel";

import logger from "@shared/logger";
import Roles from "@entities/Roles";
import {CustomError} from "../error/CustomError";
import {EMPTY_MANDATORY_FIELD} from "../error/errors";

export default class RoleService implements IRole {

    public async create(roleModel: RoleModel): Promise<RoleModel> {

        logger.info(`Creating role based on ${JSON.stringify(roleModel)}`);

        if (!roleModel || !roleModel.name || !roleModel.type) {
            throw new CustomError(`Invalid role creation attribute!`, EMPTY_MANDATORY_FIELD);
        }

        const roles = await Roles.create(roleModel);
        logger.info(`Successfully saved role model ${roleModel.name}!`)
        return Promise.resolve(roleModel);
    }

    // public async disable(id: number): Promise<string> {
    //
    //     logger.info(`Disabling role id ${id}`);
    //
    //     if (!id || id <= 0) {
    //         throw new CustomError(`Invalid id ${id} to disable`, EMPTY_MANDATORY_FIELD);
    //     }
    //
    //     await Roles.update({}, {
    //         where: {
    //             id: roleModel.id
    //         }
    //     })
    //
    //     return Promise.resolve("");
    // }

    public async update(roleModel: RoleModel): Promise<RoleModel> {

        logger.info(`Updating role based on ${JSON.stringify(roleModel)}`);
        if (!roleModel || !roleModel.id || roleModel.id <= 0) {
            throw new CustomError(`Invalid model attributes while updating!`,
                EMPTY_MANDATORY_FIELD);
        }

        await Roles.update(roleModel, {
            where: {
                id: roleModel.id
            }
        })
        logger.info(`Successfully updated role entity ${roleModel.id}`);
        return Promise.resolve(roleModel);
    }

}