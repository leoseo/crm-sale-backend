import AccountModel from "@models/AccountModel";
import TokenModel from "@models/TokenModel";

export interface IAccount {
    registerAccount: (accountModel: AccountModel) => Promise<AccountModel>;
    login: (username: string, password: string) => Promise<TokenModel| null>;
    refreshToken: (refreshToken: string, username: string) => Promise<TokenModel | null>;
}