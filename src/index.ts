import './pre-start'; // Must be the first import
import logger from '@shared/logger';
import dotenv from 'dotenv';
import {connectionString, dbSequelize} from "@shared/sequelize.connection";

import startApolloServer from "./graphql.apollo.server";

// get config vars
if (process.env.NODE_ENV !== 'production') {
    dotenv.config();
}

const exposedPort = Number (process.env.PORT || 4041);
try {
    dbSequelize.authenticate().then(() => {
        logger.info(`Connection has been established successfully at ${connectionString}`);
        startApolloServer().then(servers => {
            // Start the Express server applying middleware for Apollo Server
            const {app} = servers;
            app.listen({port : exposedPort } , () => {
                logger.info('Express & Apollo Server ready at: ' + exposedPort);
            });
        })

    });
} catch (error) {
    logger.error(`Unable to connect to the database at ${connectionString}
     or hosting Apollo server at port ${Number(process.env.PORT || 4041)}  with error:`, error);
}
