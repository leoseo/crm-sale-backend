import AccountModel from "../models/AccountModel";
import RoleModel from "@models/RoleModel";

export const books = [
    {
        title: 'The Awakening',
        author: 'Kate Chopin',
    },
    {
        title: 'City of Glass',
        author: 'Paul Auster',
    },
];

export const account: AccountModel =
    {
        fullName: 'dang le son',
        email: 'asdad@gmail.com',
        phone: '09231231',
        username: 'sondl',
        password: 'asdasdas',
        department: 'department 1',
        accStatus: 'active'
    };

export const accounts: AccountModel[] = [
    account,
    account
]

export const role: RoleModel = {
    id: 1,
    name: "Admin",
    type: 1,
    description: 'Administrator',
    createdAt: '2021-04-07 22:03:30',
    updatedAt: '2021-04-07 22:03:30',
    deletedAt: '2021-04-07 22:03:30',
}
export const roles: RoleModel[] = [
    role,
    role
]