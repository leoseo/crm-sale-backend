import {gql} from 'apollo-server-express';

const roleTypeDefs = gql`

    type Role {
        id: Float
        name: String
        type: Int
        description: String
        createdAt: String
        updatedAt: String
        deletedAt: String
    }
    
    input RoleModel {
        id: Int
        name: String
        type: Int
        description: String
        createdAt: String
        updatedAt: String
        deletedAt: String
    }
      
    type Query {
        roles: [Role]
    }
    
    type Mutation {
        createRole(roleModel: RoleModel!): Role
        updateRole(roleModel: RoleModel!): Role
    }
`;

export default roleTypeDefs;