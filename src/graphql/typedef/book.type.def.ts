import {gql} from 'apollo-server-express';

const bookTypeDefs = gql`
    type Book {
      title: String
      author: String
    }
      
    type Query {
      books: [Book]
    }
    
`;


export default bookTypeDefs;