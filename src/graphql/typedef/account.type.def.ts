import {gql} from 'apollo-server-express';

const accountTypeDefs = gql`

    type Account {
        fullName: String
        email: String
        phone: String
        username: String
        password: String
        department : String
        accStatus : String
    }
    
    type Token {
        username: String
        accessToken: String
        tokenType: String
        refreshToken: String
        scope: String
        expiresIn: Int
    }
      
    type Query {
        accounts: [Account]
    }
    
    type Mutation {
        registerAccount(fullName: String, email: String, phone: String, username: String,
         password: String, department: String, accStatus: String): Account
        login(username: String, password: String): Token
        refreshToken(username: String, refreshToken: String): Token
    }
`;

export default accountTypeDefs;