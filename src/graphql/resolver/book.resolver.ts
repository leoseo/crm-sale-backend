import {books} from "../sample.data";

// Resolvers define the technique for fetching the types defined in the
// graphql. This resolver retrieves books from the "books" array above.
export const bookResolvers = {
    Query: {
        books: () => books,
    }
};

export default bookResolvers;