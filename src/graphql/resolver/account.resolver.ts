// Resolvers define the technique for fetching the types defined in the
// graphql. This resolver retrieves books from the "books" array above.
import AccountService from "../../services/account.service";
import AccountModel from "../../models/AccountModel";
import {accounts} from "../sample.data";
import TokenModel from "@models/TokenModel";

const accountService = new AccountService();

export const accountResolvers = {
    Query: {
        accounts: () => accounts
    },
    Mutation: {
        registerAccount: async (parent: any,
                                {fullName, email, phone, username, password,
                                    department, accStatus}: AccountModel,
                                context: any, info: any) =>
            accountService.registerAccount(
                new AccountModel(fullName, email, phone,
                    username, password, department, accStatus)
            ),
        // login: async (parent: any, {username, password}: AccountModel,
        //               context: any, info: any) => accountService.login(username, password),
        // refreshToken: async (parent: any, {username, refreshToken}: TokenModel,
        //               context: any, info: any) => accountService.login(refreshToken, username)
    }
};

export default accountResolvers;