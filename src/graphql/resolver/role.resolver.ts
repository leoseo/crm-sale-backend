import {roles} from "../sample.data";
import RoleModel from "@models/RoleModel";

import RoleService from "@services/role.service";
import logger from "@shared/logger";
import {RoleModelWrapper} from "@models/RoleModelWrapper";

const roleService = new RoleService();

export const roleResolvers = {
    Query: {
        roles: () => roles
    },
    Mutation: {
        createRole: async (parent: any, roleModelWrapper: RoleModelWrapper,
                           context: any, info: any) => {
            return roleService.create(roleModelWrapper.roleModel);
        },
        updateRole: async (parent: any, roleModelWrapper: RoleModelWrapper,
                           context: any, info: any) =>
            roleService.update(roleModelWrapper.roleModel)
    }
};

export default roleResolvers;