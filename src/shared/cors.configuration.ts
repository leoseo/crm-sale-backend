import configSettings from "../pre-start/cors-url.json";
import {CorsOptions} from "apollo-server-express";

interface CorsSetting {
    urls: {
        api: string,
        app: string
    }
}
const settings: CorsSetting = configSettings;

// from @types/cors/index.d.ts
type StaticOrigin = boolean | string | RegExp | (boolean | string | RegExp)[];
const urlsAllowedToAccess: string[] =
    Object.entries(settings.urls || {}).map(([key, value]) => value) || [];

export const corsConfiguration: CorsOptions = {
    origin: (origin: string | undefined,
             callback: (err: Error | null, origin?: StaticOrigin) => void) => {
        //if (urlsAllowedToAccess.indexOf(req.header('Origin')) !== -1) {
        if (!origin || urlsAllowedToAccess.includes(origin)) {
            callback(null, true);
        } else {
            callback(new Error(`${origin} not permitted by CORS policy.`));
        }
    },
    credentials: true,
    preflightContinue: true,
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS"
};