import jwt, {JwtPayload} from 'jsonwebtoken'

import {v5 as uuidv5} from 'uuid';
import {EMPTY} from "@shared/constants";

// eslint-disable-next-line max-len
const SECRET = '2dc78014566968859703011097853a21175a9d6f12cd23b83705558c528c21a3a844b41d96d6ba9c9925295a8a1f429e1d9ed8c25b0bf721b0784ccd0aa9bcc7'
const MY_NAMESPACE = '5b160bad-dd02-4d64-a935-da07b0c977b4';

export const TOKEN_TYPE = 'Bearer'
export const EXPIRES_IN_SECONDS = 1800;
const expiresIn = `${EXPIRES_IN_SECONDS.toString()}s`;

export const generateAccessToken = async (username: string): Promise<string> => {
    const token: string = jwt.sign({username: username}, SECRET, {expiresIn: expiresIn});
    return Promise.resolve(token);
}

export const verifyAuthorizationToken =
    async (authorizationToken: string): Promise<JwtPayload | string> => {
        return Promise.resolve(jwt.verify(authorizationToken.replace(TOKEN_TYPE, EMPTY).trim(),
            SECRET));
    }

export const generateRefreshToken = async (username: string): Promise<any> => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    return Promise.resolve(uuidv5(username, MY_NAMESPACE))
}