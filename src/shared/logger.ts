/**
 * Setup the jet-logger.
 *
 * Documentation: https://github.com/seanpmaxwell/jet-logger
 */

// import Logger from 'jet-logger';
//
//
// const logger = new Logger();
//
// export default logger;


/* Winston logging*/
// const winston = require('winston');
import DailyRotateFile from 'winston-daily-rotate-file';
import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, printf } = format;
const logPath = process.env.JET_LOGGER_FILEPATH || '/var/log';

// eslint-disable-next-line @typescript-eslint/no-unsafe-call
const transport = new transports.DailyRotateFile(
    {
        filename: logPath + '/crm-sale-backend-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '40m',
        maxFiles: '14d'
    }
);

const myFormat = printf(({ level, message, label, timestamp}) => {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    return `${timestamp} [${label}] ${level}: ${message}`;
});

// transport.on('rotate', function(oldFilename, newFilename) {
//     // do something fun -> do nothing for now
// });

const logger = createLogger({
    transports: [
        new transports.Console(),
        new DailyRotateFile(transport)
    ],
    format: combine(
        label({ label: 'Snapshot-Service' }),
        timestamp(),
        myFormat
    ),
});

export default logger;
