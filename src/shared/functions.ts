export const getRandomInt = () => {
    return Math.floor(Math.random() * 1_000_000_000_000);
};

export const formatCacheKey = (username: string) => {
    return `rt_${username}`;
}
